package ua.lokha.webmoneycalculate;

import java.util.ArrayList;
import java.util.List;

import static ua.lokha.webmoneycalculate.Utils.sumPrice;
import static ua.lokha.webmoneycalculate.Utils.sumProduct;

public class Calculate {

    private List<Offer> offers;
    private double find;
    private boolean duplicate;

    public Calculate(List<Offer> offers, double find) {
        this.offers = offers;
        this.find = find;
    }

    public List<Offer> max() {
        List<Offer> max = null;
        for (Offer offer : offers) {
            if (offer.getPrice() > find) {
                continue;
            }
            List<Offer> check = new ArrayList<>(1);
            check.add(offer);
            List<Offer> result = max0(check);
            if (max == null || sumProduct(result) > sumProduct(max)) {
                max = result;
            }
        }
        return max;
    }

    private List<Offer> max0(List<Offer> check) {
        System.out.println("check " + check + " offers " + offers);
        if (sumPrice(check) > find) {
            throw new IllegalArgumentException("check sum " + sumPrice(check) + " > find " + find);
        }

        List<Offer> toCheck;
        if (!duplicate) {
            toCheck = new ArrayList<>(this.offers);
            toCheck.removeAll(check);
        } else {
            toCheck = this.offers;
        }
        List<Offer> result = check;
        for (Offer offer : toCheck) {
            List<Offer> checkNext = new ArrayList<>(check.size() + 1);
            checkNext.addAll(check);
            checkNext.add(offer);
            if (sumPrice(checkNext) > find) {
                continue;
            }
            List<Offer> resultNext = max0(checkNext);
            if (sumProduct(resultNext) > sumProduct(result)) {
                result = resultNext;
            }
        }

        return result;
    }


    public void setDuplicate(boolean duplicate) {
        this.duplicate = duplicate;
    }
}
