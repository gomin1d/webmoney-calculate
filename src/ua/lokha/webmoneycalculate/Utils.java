package ua.lokha.webmoneycalculate;

import java.util.List;

public class Utils {

    public static double sumPrice(List<Offer> offers) {
        return offers.stream().mapToDouble(Offer::getPrice).sum();
    }

    public static double sumProduct(List<Offer> offers) {
        return offers.stream().mapToDouble(Offer::getProduct).sum();
    }

}
