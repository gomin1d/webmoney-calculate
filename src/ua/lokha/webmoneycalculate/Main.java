package ua.lokha.webmoneycalculate;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static ua.lokha.webmoneycalculate.Utils.sumPrice;
import static ua.lokha.webmoneycalculate.Utils.sumProduct;


public class Main {

    public static void main(String[] args) throws Exception {
        // загружает данные из data.txt
        // туда нужно вставить данные с сайта биржи webmoney, вот так: https://imgur.com/a/2sJ6pCd
        List<Offer> offers = loadOffers();

        // сколько денег хотим вывести, система подберет самые выгодные предложения,
        // чтобы вывести указанное число денег
        double find = 5411;

        // допустимый порог при подборе предложений
        // иногда есть выгодные предложения, но на них можно не хватить денег (буквально, 10, например)
        // можно указать, сколько вы готовы заплатить, если нужно будет
        // если вы хотите вывести ровно сумму find и не цифрой больше, тогда укажите 0
        double threshold = 0;

        Calculate calculate = new Calculate(offers, find + threshold);
        calculate.setDuplicate(false);
        List<Offer> max = calculate.max();

        if (max == null) {
            System.out.println("Выгодное предложение не найдено.");
        } else {
            System.out.println("Самое выгодное: ");
            for (Offer offer : max) {
                System.out.println(" Offer: " + offer);
            }
            System.out.println("  Цена и продукт: " + sumPrice(max) + " - " + sumProduct(max));
            System.out.println("  Точность с искомым: " + sumPrice(max) + " - " + find + " = " + (sumPrice(max) - find));
        }
    }

    private static List<Offer> loadOffers() throws IOException {
        List<Offer> offers = new ArrayList<>();

        String data = FileUtils.readFileToString(new File("data.txt"));
        for (String line : data.split("[\n\r]+")) {
            String[] field = line.split("[\t ]+");

            double price = Double.parseDouble(field[0].replace(",", "."));
            double product = Double.parseDouble(field[1].replace(",", "."));
            offers.add(new Offer(price, product));
        }
        return offers;
    }
}
