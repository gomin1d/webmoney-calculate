package ua.lokha.webmoneycalculate;

public class Offer {
    private double price;
    private double product;

    public Offer(double price, double product) {
        this.price = price;
        this.product = product;
    }

    public double getPrice() {
        return price;
    }

    public double getProduct() {
        return product;
    }

    @Override
    public String toString() {
        return "Offer{" +
                "price=" + price +
                ", product=" + product +
                '}';
    }
}